using System;
using System.Collections.Generic;
using UnityEngine;

namespace NSActiveZone
{
    [RequireComponent(typeof(PolygonCollider2D))]
    public abstract class AbstractObject2D : MonoBehaviour
    {
        #region members
        
        /// <summary>
        /// Zona activa propia del objeto, sirve para indicarle al usuario si este objeto se puede seleccionar
        /// </summary>
        [Tooltip("Zona activa propia del objeto, sirve para indicarle al usuario si este objeto se puede seleccionar")]
        [SerializeField] private ActiveZoneOnWorld activeZoneOwn;

        /// <summary>
        /// Todas las zonas activas relacionadas con este objeto
        /// </summary>
        [Tooltip("Todas las zonas activas relacionadas con este objeto")]
        [SerializeField] private ActiveZoneOnWorld[] arrayActiveZones;

        /// <summary>
        /// Saber si el objeto esperando por interaccion o se esta moviendo
        /// </summary>
        protected ActualAction actualAction;

        /// <summary>
        /// Para acceder facilmente por nombre a cualquiera de las zonas activas
        /// </summary>
        private Dictionary<string, ActiveZoneOnWorld> dictionaryActiveZones;

        /// <summary>
        /// Para saber en que etapa esta el objeto y poder hacer tareas en especifico segun la etapa
        /// </summary>
        protected float actualStageIndex;

        /// <summary>
        /// Lista que contiene todas las zonas activas relacionadas con la etapa actual del objeto, esta lista se llena cuando el objeto se selecciona
        /// </summary>
        private List<ActiveZoneOnWorld> listActiveZonesRelatedWithStageIndex = new List<ActiveZoneOnWorld>();

        /// <summary>
        /// Posicion inicial del objeto 2D
        /// </summary>
        private Vector2 initPosition;

        /// <summary>
        /// Posicion del touch en el mundo, y posicion hacia donde el objeto deberia moverses
        /// </summary>
        private Vector2 positionObjective;

        /// <summary>
        /// Desface desde el centro del objeto hasta la posicion del touch
        /// </summary>
        private Vector2 positionOffsetFromTouch;

        /// <summary>
        /// Zona activa mas cercana
        /// </summary>
        private ActiveZoneOnWorld refActiveZoneOnWorldMoreNear;

        /// <summary>
        /// Para calcular colisiones con las zonas activas que son areas
        /// </summary>
        private PolygonCollider2D refPolygonCollider2D;

        private const float VelocityMoveTowardActiveZone = 0.2f;

        protected bool activateActiveZonesRelatedWithStage = true;

        #endregion

        #region properties

        /// <summary>
        /// Asignar la posicion activa
        /// </summary>
        public Vector2 PositionObjective
        {
            get { return positionObjective; }
            set { positionObjective = value; }
        }

        /// <summary>
        /// Saber si el objeto esta esperando por interaccion o se esta moviendo al punto inicial o a otro punto
        /// </summary>
        public ActualAction ActualAction
        {
            get { return actualAction; }
        }

        /// <summary>
        /// Indice de la etapa actual, sirve para definir como debe comportarse el objeto en determinada etapa
        /// </summary>
        public float ActualStageIndex
        {
            get { return actualStageIndex; }
            set { actualStageIndex = value; }
        }

        #endregion

        #region MonoBehaviour

        protected virtual void Awake()
        {
            InitDictionaryActiveZones();
            initPosition = transform.position;
            refPolygonCollider2D = GetComponent<PolygonCollider2D>();
            refPolygonCollider2D.enabled = false;

            if (activeZoneOwn)
                activeZoneOwn.GetComponent<PolygonCollider2D>().enabled = false;
        }

        protected virtual void Update()
        {
            MoveTowardsInitPosition();
        }

        #endregion

        #region private methods

        private void InitDictionaryActiveZones()
        {
            if (dictionaryActiveZones == null)
            {
                dictionaryActiveZones = new Dictionary<string, ActiveZoneOnWorld>();

                foreach (var tmpItem in arrayActiveZones)
                {
                    dictionaryActiveZones.Add(tmpItem.name, tmpItem);
                    tmpItem.delegateOnTab += OnTabOverActiveZone;
                }
            }
        }

        /// <summary>
        /// Moverse hacia la posicion inicial
        /// </summary>
        private void MoveTowardsInitPosition()
        {
            if (actualAction == ActualAction.MovingToInitPosition)
            {
                transform.position = Vector2.MoveTowards(transform.position, initPosition, 0.5f);

                if (Math.Abs(Vector2.Distance(transform.position, initPosition)) < 0.005f)
                    actualAction = ActualAction.WaitingForAction;
            }
        }

        /// <summary>
        /// Consigue la zona activa mas cercana
        /// </summary>
        private ActiveZoneOnWorld GetActiveZoneMostNear()
        {
            ActiveZoneOnWorld tmpActiveZoneOnWorldMoreNear = null;
            var tmpMinDistance = Mathf.Infinity;

            foreach (var tmpActiveZone in listActiveZonesRelatedWithStageIndex)
            {
                var tmpMinDistanceNew = tmpActiveZone.GetDistanceToActiveZone(positionObjective + positionOffsetFromTouch, refPolygonCollider2D);

                if (tmpMinDistanceNew < tmpMinDistance)
                {
                    tmpActiveZoneOnWorldMoreNear = tmpActiveZone;
                    tmpMinDistance = tmpMinDistanceNew;
                }
            }

            return tmpActiveZoneOnWorldMoreNear;
        }

        /// <summary>
        /// Se ejecuta cuando se suelta este objeto sobre una zona activa
        /// </summary>
        /// <param name="argNameActiveZone">Nombre de la zona activa sobre la cual se suelta este objeto</param>
        protected abstract void OnReleaseObjectOverActiveZone(string argNameActiveZone);

        /// <summary>
        /// Se ejecuta cuando se suelta este objeto y no esta sobre una zona activa
        /// </summary>
        protected abstract void OnReleaseObjectOverNothing();

        /// <summary>
        /// Notifica un tab/click sobre este objeto
        /// </summary>
        public abstract void OnTab();

        /// <summary>
        /// Notifica un /click sobre una de las zonas activas de este objeto
        /// </summary>
        /// <param name="argNameActiveZone">Nombre de la zona activa de este objeto sobre la cual se hizo tab/click </param>
        public abstract void OnTabOverActiveZone(string argNameActiveZone);

        /// <summary>
        /// Notifica cuando un objeto se esta moviendo
        /// </summary>
        public abstract void OnMoving();

        #endregion

        #region Interaccion con el selector, solo modifique este codigo si sabe lo que hace.

        /// <summary>
        /// Se ejecuta cuando se selecciona el objeto con el touch lean
        /// </summary>
        public void OnSelected()
        {
            positionOffsetFromTouch = (Vector2) transform.position - positionObjective;

            foreach (var tmpActiveZone in dictionaryActiveZones.Values)
                if (tmpActiveZone.ActiveZoneWhenSelectObject && tmpActiveZone.ActiveZoneRelatedIndexWithStage == actualStageIndex)
                    listActiveZonesRelatedWithStageIndex.Add(tmpActiveZone);
        }

        /// <summary>
        /// Se ejecuta cuando se esta moviendo el touch sobre la pantalla
        /// </summary>
        public void OnMovement()
        {
            if (activateActiveZonesRelatedWithStage)
            {
                foreach (var tmpActiveZone in listActiveZonesRelatedWithStageIndex)
                    if (tmpActiveZone.ActiveZoneWhenSelectObject)
                        tmpActiveZone.ActiveActiveZone();

                activateActiveZonesRelatedWithStage = false;
            }

            actualAction = ActualAction.InMovement;
            refActiveZoneOnWorldMoreNear = GetActiveZoneMostNear(); //Cual es la zona activa mas cercana?

            var tmpPosition = transform.position;

            if (refActiveZoneOnWorldMoreNear != null)
            {
                if (refActiveZoneOnWorldMoreNear.IsActiveZoneArea)
                    transform.position = Vector2.MoveTowards(tmpPosition, positionObjective + positionOffsetFromTouch, 0.9f);
                else
                    transform.position = Vector2.MoveTowards(tmpPosition, refActiveZoneOnWorldMoreNear.GetPointOverActiveZone(positionObjective + positionOffsetFromTouch), VelocityMoveTowardActiveZone);
            }
            else
                transform.position = Vector2.MoveTowards(tmpPosition, positionObjective + positionOffsetFromTouch, 0.9f);

            OnMoving();
        }

        /// <summary>
        /// Se ejecuta cuando se deselecciona el objeto con el touch lean
        /// </summary>
        public void OnSelectFinish()
        {
            if (!activateActiveZonesRelatedWithStage)
            {
                foreach (var tmpActiveZone in listActiveZonesRelatedWithStageIndex)
                    tmpActiveZone.ActiveActiveZone(false);

                activateActiveZonesRelatedWithStage = true;
            }

            listActiveZonesRelatedWithStageIndex.Clear();
            actualAction = ActualAction.MovingToInitPosition;

            if (refActiveZoneOnWorldMoreNear != null)
            {
                initPosition = refActiveZoneOnWorldMoreNear.GetPointOverActiveZone(transform.position);
                OnReleaseObjectOverActiveZone(refActiveZoneOnWorldMoreNear.name);
            }
            else
                OnReleaseObjectOverNothing();

            refActiveZoneOnWorldMoreNear = null;
        }

        #endregion

        #region public methods

        /// <summary>
        /// Activa la animacion de una zona activa en especifico
        /// </summary>
        /// <param name="argNameActiveZone">Nombre de la zona activa</param>
        /// <param name="argActivate">Activar animacion?</param>
        public void ActivateActiveZone(string argNameActiveZone, bool argActivate = true)
        {
            if (dictionaryActiveZones.ContainsKey(argNameActiveZone))
            {
                var tmpActiveZone = dictionaryActiveZones[argNameActiveZone];
                tmpActiveZone.ActiveActiveZone(argActivate);
            }
            else
                Debug.LogError("ZonaActiva : " + argNameActiveZone + " |del objeto : " + name + " No existe");
        }

        /// <summary>
        /// Activa la zona activa con un is active = 1 o -1 unicamente
        /// </summary>
        /// <param name="argNameActiveZone">Nombre de la zona activa</param>
        /// <param name="argActivate">Activar animacion?</param>
        public void ActivateActiveZoneFixed(string argNameActiveZone, bool argActivate = true)
        {
            if (dictionaryActiveZones.ContainsKey(argNameActiveZone))
            {
                var tmpActiveZone = dictionaryActiveZones[argNameActiveZone];
                tmpActiveZone.ActiveActiveZoneFixed(argActivate);
            }
            else
                Debug.LogError("ZonaActiva : " + argNameActiveZone + " |del objeto : " + name + " No existe");
        }

        /// <summary>
        /// Activar o desactivar la zona activa propia del objeto
        /// </summary>
        /// <param name="argActivar">Activar?</param>
        public void ActivateActiveZoneOwn(bool argActivar = true)
        {
            refPolygonCollider2D.enabled = argActivar;

            if (activeZoneOwn)
                activeZoneOwn.ActiveActiveZoneFixed(argActivar);
            else
                Debug.LogError("No tiene zona activa propia");
        }

        /// <summary>
        /// Actualiza la etapa de una zona activa en especifico
        /// </summary>
        /// <param name="argNameActiveZone">Nombre de la zona activa</param>
        /// <param name="argNewStage">Nueva etapa</param>
        public void UpdateStageActiveZone(string argNameActiveZone, int argNewStage)
        {
            if (dictionaryActiveZones.ContainsKey(argNameActiveZone))
                dictionaryActiveZones[argNameActiveZone].ActiveZoneRelatedIndexWithStage = argNewStage;
            else
                Debug.LogError("ZonaActiva : " + argNameActiveZone + " |del objeto : " + name + " No existe");
        }

        /// <summary>
        /// Activa o desactiva todas las zonas activas relacioandas con la etapa actual
        /// </summary>
        /// <param name="argActivate">Activar?</param>
        public void ActivateAllActiveZonesOfActualStage(bool argActivate)
        {
            foreach (var tmpActiveZone in arrayActiveZones)
                if (actualStageIndex == tmpActiveZone.ActiveZoneRelatedIndexWithStage)
                    tmpActiveZone.ActiveActiveZone(argActivate);
        }
        
        /// <summary>
        /// Agrega una zona activa como punto objetivo al que se puede mover este objeto
        /// </summary>
        /// <param name="argNameActiveZone">Nombre de la zona activa a agregar</param>
        /// <param name="argAdd">Agregar? o quitar?</param>
        public void AddActiveZoneListActiveZones(string argNameActiveZone, bool argAdd = true)
        {
            if (dictionaryActiveZones.ContainsKey(argNameActiveZone))
            {
                var tmpActiveZone = dictionaryActiveZones[argNameActiveZone];

                if (argAdd)
                {
                    if (!listActiveZonesRelatedWithStageIndex.Contains(tmpActiveZone))
                        listActiveZonesRelatedWithStageIndex.Add(tmpActiveZone);
                }
                else
                {
                    if (listActiveZonesRelatedWithStageIndex.Contains(tmpActiveZone))
                        listActiveZonesRelatedWithStageIndex.Remove(tmpActiveZone);
                }
            }
        }

        #endregion
    }

    public enum ActualAction
    {
        WaitingForAction,
        InMovement,
        MovingToInitPosition
    }
}