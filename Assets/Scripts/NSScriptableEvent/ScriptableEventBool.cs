using UnityEngine;

namespace NSScriptableEvent
{
    [CreateAssetMenu(fileName = "Event Bool", menuName = "NSScriptableEvent/Bool", order = 0)]
    public class ScriptableEventBool : AbstractScriptableEvent<bool>
    {
        
    }
}