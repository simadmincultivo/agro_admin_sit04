using UnityEngine;

namespace NSScriptableEvent
{
    [CreateAssetMenu(fileName = "Event float", menuName = "NSScriptableEvent/Float", order = 0)]
    public class ScriptableEventFloat : AbstractScriptableEvent<float>
    {
        
    }
}