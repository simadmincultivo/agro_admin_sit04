#pragma warning disable 0649
using System;
using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent
{
    /// <summary>
    /// Representacion abstracta de un scriptable event
    /// </summary>
    /// <typeparam name="T">Cualquier tipo de valor que se quiera pasar por argumento para el evento</typeparam>
    public abstract class AbstractScriptableEvent<T> : ScriptableObject
    {
        protected UnityEventT<T> actionEvent = new UnityEventT<T>();
        
        [Tooltip("Show debug code?")]
        [SerializeField] private bool showDebug;

        /// <summary>
        /// Suscribir un evento 
        /// </summary>
        /// <param name="argMethodSubscribe"></param>
        public void Subscribe(UnityAction<T> argMethodSubscribe)
        {
            Debug.Log("actionEvent : "+actionEvent.GetType());
            actionEvent.AddListener(argMethodSubscribe);

            if (showDebug)
                Debug.Log("Subscribe Method : " + argMethodSubscribe.Method.Name + " To scriptable event :" + name);
        }

        public void Unsubscribe(UnityAction<T> argMethodUnsubscribe)
        {
            actionEvent.RemoveListener(argMethodUnsubscribe);

            if (showDebug)
                Debug.Log("Unsubscribe Method : " + argMethodUnsubscribe.Method.Name+ " To scriptable event :" + name);
        }

        public void ExecuteEvent(T argValue)
        {
            actionEvent.Invoke(argValue);

            if (showDebug)
                Debug.Log("Execute event : " + name);
        }
    }
    
    [CreateAssetMenu(fileName = "Event ", menuName = "NSScriptableEvent/Event", order = 0)]
    public class ScriptableEvent : ScriptableObject
    {
        [SerializeField] protected UnityEvent actionEvent;

        [SerializeField] private bool showDebug = false;

        public void Subscribe(UnityAction argMethodSubscribe)
        {
            actionEvent.AddListener(argMethodSubscribe);

            if (showDebug)
                Debug.Log("Subscribe Method : " + argMethodSubscribe.Method.Name + " To scriptable event :" + name);
        }

        public void Unsubscribe(UnityAction argMethodUnsubscribe)
        {
            actionEvent.RemoveListener(argMethodUnsubscribe);

            if (showDebug)
                Debug.Log("Unsubscribe Method : " + argMethodUnsubscribe.Method.Name + " To scriptable event :" + name);
        }

        public void ExecuteEvent()
        {
            actionEvent.Invoke();

            if (showDebug)
                Debug.Log("Execute event : " + name);
        }
    }
    
    public class UnityEventT<T> : UnityEvent<T>
    {
        
    }
}