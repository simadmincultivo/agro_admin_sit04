using UnityEngine;

namespace NSScriptableEvent
{
    [CreateAssetMenu(fileName = "Event Vector3", menuName = "NSScriptableEvent/Vector3", order = 0)]
    public class ScriptableEventVector3 : AbstractScriptableEvent<Vector3>
    {
        
    }
}