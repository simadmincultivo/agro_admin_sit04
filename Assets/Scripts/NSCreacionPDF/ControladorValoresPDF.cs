﻿using System;
using NSCalificacionSituacion;
using System.Collections;
using System.Collections.Generic;
using NSScriptableEvent;
using NSScriptableValues;
using NSSeguridad;
using UnityEngine;
using UnityEngine.UI;
using NSTraduccionIdiomas;
using TMPro;
using UnityEngine.Serialization;

namespace NSCreacionPDF
{
    /// <summary>
    /// Clase que contiene todos los textos UI y objetos UI del PDF que necesitan asignacion de valores con respecto a lo que sucedio en el simulador
    /// </summary>
    [RequireComponent(typeof(CanvasReportePDF))]
    public class ControladorValoresPDF : MonoBehaviour
    {
        #region members
        [Header("Valores y datos del usuario"), Space(10)]
        [SerializeField] private string nombreSituacion;

        [SerializeField] private ValueString valFloatScore;
        
        [SerializeField] private SOSecurityData soSecurityData;

        [SerializeField] private SOSessionData soSessionData;

        [SerializeField] private TextMeshProUGUI textUsuarioValor;

        [SerializeField] private TextMeshProUGUI textInstitucionValor;

        [SerializeField] private TextMeshProUGUI textSituacionValor;

        [SerializeField] private TextMeshProUGUI textCursoValor;

        [SerializeField] private TextMeshProUGUI textUnidadValor;

        [SerializeField] private TextMeshProUGUI textIDCursoValor;

        [SerializeField] private TextMeshProUGUI textFechaInicioValor;

        [SerializeField] private TextMeshProUGUI textTiempoPracticaValor;

        [SerializeField] private TextMeshProUGUI textIntentosValor;

        [SerializeField] private TextMeshProUGUI textCalificacionValor;

        /// <summary>
        /// transform padre donde deben ser creados los registros de datos
        /// </summary>
        [SerializeField, Header("Registro Datos situacion seleccionada"), Space(10)]
        private Transform contenedorRegistroDatos;
        
        /// <summary>
        /// Panel registro datos en el canvas de la situacion
        /// </summary>
        [Tooltip("Panel registro datos en el canvas de la situacion")]
        [SerializeField] private GameObject panelRegistroDatos;
        
        [SerializeField] private float sizePanelRegistroDatosScale = 0.5f;

        [SerializeField, Header("Preguntas evaluacion"), Space(10)]
        private Image imageEnunciadoEvaluacion;

        [SerializeField] private TextMeshProUGUI textEnunciadoEvaluacion;

        /// <summary>
        /// transform padre donde deben ser creadas las preguntas de evaluacion
        /// </summary>
        [SerializeField] private Transform transformPreguntasEvaluacion;

        /// <summary>
        /// transform padre donde deben ser creadas las respuestas del usuario 
        /// </summary>
        [SerializeField] private Transform transformListaRespuestasUsuario;

        /// <summary>
        /// transform padre donde deben ser creadas las respuestas correctas de la evaluacion
        /// </summary>
        [SerializeField] private Transform transformListaRespuestasCorrectas;

        [SerializeField] private GameObject prefabTextPreguntaRespuesta;

        [Header("Cuaderno"), Space(10)]
        [SerializeField] private TextMeshProUGUI textPreguntasCuaderno1;

        [SerializeField] private ScriptableEventArrayString seQuestionsReady;
        
        #endregion

        #region Mono behaviour

        private void OnEnable()
        {
            seQuestionsReady.Subscribe(SetPreguntasCuaderno);
        }

        private void OnDisable()
        {
            seQuestionsReady.Unsubscribe(SetPreguntasCuaderno);
        }

        #endregion
        
        #region public methods

        public void SetValuesOnPDF()
        {
            textUsuarioValor.text = soSecurityData.aulaUsername;
            textInstitucionValor.text = soSecurityData.aulaSchoolName;
            textSituacionValor.text = DiccionarioIdiomas.Instance.Traducir(nombreSituacion);
            textCursoValor.text = soSecurityData.aulaClassName;/// DiccionarioIdiomas.Instance.Traducir("TextPlaceHolderCurso");
            textUnidadValor.text = DiccionarioIdiomas.Instance.Traducir("TextUnidadValue");
            textIDCursoValor.text = soSecurityData.aulaClassId;
            textFechaInicioValor.text = DateTime.Today.ToString("dd/MM/yyyy");
            textTiempoPracticaValor.text = soSessionData.TimeSituationString;
            textIntentosValor.text = soSessionData.quantityAttempts.ToString();
            textCalificacionValor.text = valFloatScore.Value;
            SetPanelRegistroDatos();
        }
        
        private void SetPanelRegistroDatos()
        {
            if (panelRegistroDatos)
            {
                var tmpPanelRegistroDatos = Instantiate(panelRegistroDatos, contenedorRegistroDatos);
                tmpPanelRegistroDatos.SetActive(true);
                tmpPanelRegistroDatos.GetComponent<CanvasGroup>().alpha = 1;
                tmpPanelRegistroDatos.transform.localScale = Vector2.one * sizePanelRegistroDatosScale; 
            }
            else
                Debug.LogError("El panel de registro de datos no a sido asignado");
        }

        public void SetPreguntasSituacion(SOPreguntasSituacion argSoPreguntasSituacion, string[] argRespuestasUsuario, Sprite[] argRespuestasImagenUsuario)
        {
            var tmpPageWhereIsQuestionsEvaluation = transform.GetChild(1).gameObject;   
            tmpPageWhereIsQuestionsEvaluation.SetActive(true);
            
            imageEnunciadoEvaluacion.sprite = argSoPreguntasSituacion.ImagenEnunciado;
            textEnunciadoEvaluacion.text = DiccionarioIdiomas.Instance.Traducir(argSoPreguntasSituacion.TextoEnunciado);
            
            //borrar preguntas antiguas
            while (transformPreguntasEvaluacion.childCount > 0)
                DestroyImmediate(transformPreguntasEvaluacion.GetChild(0).gameObject);

            //asignar preguntas nuevas.
            for (int i = 0; i < argSoPreguntasSituacion.Preguntas.Length; i++)
            {
                var tmpTextPreguntaEvaluacion = Instantiate(prefabTextPreguntaRespuesta, transformPreguntasEvaluacion).GetComponent<TextMeshProUGUI>();
                tmpTextPreguntaEvaluacion.text = DiccionarioIdiomas.Instance.Traducir(argSoPreguntasSituacion.Preguntas[i]);
                LayoutRebuilder.ForceRebuildLayoutImmediate(tmpTextPreguntaEvaluacion.GetComponent<RectTransform>());
            }

            LayoutRebuilder.ForceRebuildLayoutImmediate(transformPreguntasEvaluacion.GetComponent<RectTransform>()); 
            
            //borrar respuestas correctas antiguas
            while (transformListaRespuestasCorrectas.childCount > 0)
                DestroyImmediate(transformListaRespuestasCorrectas.GetChild(0).gameObject);

            //asignar respuestas correctas nuevas.
            for (int i = 0; i < argSoPreguntasSituacion.Respuestas.Length; i++)
            {
                var tmpTextRespuestaCorrectaEvaluacion = Instantiate(prefabTextPreguntaRespuesta, transformListaRespuestasCorrectas).GetComponent<TextMeshProUGUI>();
                tmpTextRespuestaCorrectaEvaluacion.text = DiccionarioIdiomas.Instance.Traducir(argSoPreguntasSituacion.Respuestas[i]);
                LayoutRebuilder.ForceRebuildLayoutImmediate(tmpTextRespuestaCorrectaEvaluacion.GetComponent<RectTransform>());
            }
            
            LayoutRebuilder.ForceRebuildLayoutImmediate(transformListaRespuestasCorrectas.GetComponent<RectTransform>());
            //borrar respuestas usuario antiguas
            while (transformListaRespuestasUsuario.childCount > 0)
                DestroyImmediate(transformListaRespuestasUsuario.GetChild(0).gameObject);

            //asignar respuestas usuario nuevas.
            for (int i = 0; i < argSoPreguntasSituacion.ContenedorRespuestasFalsas.Length; i++)
            {
                var tmpTextRespuestaCorrectaEvaluacion = Instantiate(prefabTextPreguntaRespuesta, transformListaRespuestasUsuario).GetComponent<TextMeshProUGUI>();
                tmpTextRespuestaCorrectaEvaluacion.text =  argRespuestasUsuario[i];
                LayoutRebuilder.ForceRebuildLayoutImmediate(tmpTextRespuestaCorrectaEvaluacion.GetComponent<RectTransform>());
            }
            
            LayoutRebuilder.ForceRebuildLayoutImmediate(transformListaRespuestasUsuario.GetComponent<RectTransform>());
            tmpPageWhereIsQuestionsEvaluation.SetActive(false);
        }

        private void SetPreguntasCuaderno(string[] argPreguntasCuaderno)
        {
            textPreguntasCuaderno1.text = "";

            foreach (var tmpPregunta in argPreguntasCuaderno)
                textPreguntasCuaderno1.text += tmpPregunta + "\n\n";
        }
        #endregion
    }
}