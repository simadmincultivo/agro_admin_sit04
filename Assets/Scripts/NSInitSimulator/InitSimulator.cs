﻿﻿#pragma warning disable 0649
using System;
using NSBoxMessage;
using NSInterfaz;
using NSScriptableEvent;
using NSScriptableValues;
using NSSeguridad;
using NSTraduccionIdiomas;
using UnityEngine;
using NSAvancedUI;
using NSInterfaz;

namespace NSInitSimulator
{
    public class InitSimulator : MonoBehaviour
    {
        [Header("Dictionary")] 
        [SerializeField] private IdiomsList defaultIdiom;
        
        [SerializeField] private ScriptableEvent seOnDictionaryLoaded;

        [Tooltip("¿Tiene el simulador ventana para la seleccion de idiomas?")]
        [SerializeField] public ValueBool simulatorIsBilingual;
        
        [SerializeField] private ScriptableEvent seOnPanelSelectIdiomClose;
        
        [Header("Login")]
        [SerializeField] private ScriptableEvent seOnPanelWellcomeClose;

        private void Awake()
        {
            seOnDictionaryLoaded.Subscribe(OnDictionaryLoaded);
            seOnPanelSelectIdiomClose.Subscribe(OnPanelSelectIdiomClose);
            DiccionarioIdiomas.CreateInstance();
            DiccionarioIdiomas.Instance.SetIdiom(defaultIdiom);
        }

        private void OnDictionaryLoaded()
        {
            if (simulatorIsBilingual)
                PanelInterfazSeleccionLenguajeInglesEspaniol.Instance.ShowPanel();
            else
                seOnPanelWellcomeClose.ExecuteEvent();
        }

        public void OnPanelSelectIdiomClose()
        {
            PanelInterfazSeleccionLenguajeInglesEspaniol.Instance.ShowPanel(false);
            BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("TextBienvenida"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarPanelWellcome);
        }
        
        private void OnButtonAceptarPanelWellcome()
        {
            seOnPanelWellcomeClose.ExecuteEvent();
        }
    }
}