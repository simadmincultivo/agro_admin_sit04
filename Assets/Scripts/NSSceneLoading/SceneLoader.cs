
using System;
using NSSingleton;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace NSSceneLoading
{
    public class SceneLoader : AbstractSingleton<SceneLoader>
    {
        #region members

        private Scene activeScene;
        
        private Scene previousScene;
        #endregion
        
        #region MonoBehaviour

        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
        
        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        #endregion

        #region private methods

        private void OnSceneLoaded(Scene argScene, LoadSceneMode argLoadSceneMode)
        {
            activeScene = argScene;
            SceneManager.SetActiveScene(activeScene);
            
            if (previousScene != default)
            {
                SceneManager.UnloadSceneAsync(previousScene);
                previousScene = default;
            }
        }
        
        #endregion

        #region public methods

        public void LoadScene(string argSceneName, bool argAdditiveMode = true, Scene argUnloadPreviousScene = default)
        {
            if (argAdditiveMode)
            {
                previousScene = argUnloadPreviousScene;
                SceneManager.LoadSceneAsync(argSceneName, LoadSceneMode.Additive);
            }
            else
                SceneManager.LoadSceneAsync(argSceneName, LoadSceneMode.Single);
        }

        public void UnloadScene(string argSceneName)
        {
            SceneManager.UnloadSceneAsync(argSceneName);
        }

        public void RestartScene()
        {
            LoadScene(activeScene.name, true, SceneManager.GetActiveScene());
        }
        #endregion
    }
}