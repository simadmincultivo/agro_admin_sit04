﻿using System.Collections;
using UnityEngine;
using NSScriptableValues;

public class CalificacionManagerP2 : MonoBehaviour
{
    [Header("Referencias a Scripts")]
    [SerializeField] private RegisterDatesP2 myRegisterDates2;
    [SerializeField] private CalculosCantidadProductosP2 myCalculoCantidadProductos;

    [Header("Referencia a Scriptable Objetc")]
    [SerializeField] private ValueFloat valCalificationRegisterData2;
    [SerializeField] private ValueFloat valCalificationSingleTableS2;

    public void CheckCalificationFinal()
    {
        valCalificationSingleTableS2.Value = myCalculoCantidadProductos.calificacion1;
        valCalificationRegisterData2.Value = myRegisterDates2.calificacion2;
    }
}
