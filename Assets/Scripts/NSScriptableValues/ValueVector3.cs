#pragma warning disable 0660
#pragma warning disable 0661
using UnityEngine;

namespace NSScriptableValues
{
    [CreateAssetMenu(fileName = "Vector3", menuName = "NSScriptableValue/Vector3", order = 0)]
    public class ValueVector3 : AbstractScriptableValue<Vector3>
    {
        #region sobre carga operadores

        public static implicit operator Vector3(ValueVector3 argValueA)
        {
            return argValueA.value;
        }

        public static ValueVector3 operator +(ValueVector3 argValueA, ValueVector3 argValueB)
        {
            argValueA.Value += argValueB.value;
            return argValueA;
        }

        public static ValueVector3 operator -(ValueVector3 argValueA, ValueVector3 argValueB)
        {
            argValueA.Value -= argValueB.value;
            return argValueA;
        }

        public static ValueVector3 operator *(ValueVector3 argValueA, float argValueB)
        {
            argValueA.Value *= argValueB;
            return argValueA;
        }

        public static ValueVector3 operator /(ValueVector3 argValueA, float argValueB)
        {
            argValueA.Value /= argValueB;
            return argValueA;
        }

        public static bool operator >(ValueVector3 argValueA, ValueVector3 argValueB)
        {
            return argValueA.value.magnitude > argValueB.value.magnitude;
        }

        public static bool operator <(ValueVector3 argValueA, ValueVector3 argValueB)
        {
            return argValueA.value.magnitude < argValueB.value.magnitude;
        }

        public static bool operator ==(ValueVector3 argValueA, ValueVector3 argValueB)
        {
            return argValueA.value == argValueB.value;
        }

        public static bool operator !=(ValueVector3 argValueA, ValueVector3 argValueB)
        {
            return argValueA.value != argValueB.value;
        }

        public static bool operator >=(ValueVector3 argValueA, ValueVector3 argValueB)
        {
            return argValueA.value.magnitude >= argValueB.value.magnitude;
        }

        public static bool operator <=(ValueVector3 argValueA, ValueVector3 argValueB)
        {
            return argValueA.value.magnitude <= argValueB.value.magnitude;
        }

        #endregion
    }
}