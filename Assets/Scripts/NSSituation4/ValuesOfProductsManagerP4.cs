﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValuesOfProductsManagerP4 : MonoBehaviour
{
    [Header("Datos ingresados por el usuario para la simulación")]
    public float terrainLargo;
    public float terrainAncho;
    public float tiempoCultivo;
    public float distanciaCalleMayor;
    public float distanciaPlantas;
    public int repFertilizacion;
    [System.NonSerialized] public int repRiego;
    public int repTutorado;
    public int repPodasFormacion;
    public int repPodasSanitarias;
    public int repPlagasEnfermedades;
    public int repMalezas;

    [Header("Precio de insumos")]

    public int precioAbono1;
    public int precioAbono2;
    public int precioAbono3;
    public int precioMateriaOrganica;
    public int precioInsecticida1;
    public int precioInsecticida2;
    public int precioFungicida1;
    public int precioHerbicida1;
    public int precioHerbicida2;
    public int precioTijerasGr;
    public int precioTijerasPq;
    public int precioEstaca;
    public int precioRolloCabuya;
    public int precioAgua;
    public int precioJornal;
    public int precioPlanta;

    [System.NonSerialized] public float areaCultivo;
    [System.NonSerialized] public float cantidadPlantasDensidad;
    [System.NonSerialized] public float cantidadPlantas;
    [System.NonSerialized] public float cantidadProduc;

    public void SetValuesAreaAndPlantas()
    {
        CalculateArableArea();
        CountPlantsInCrop();
    }

    void CalculateArableArea()
    {
        areaCultivo = terrainLargo * terrainAncho;
        cantidadPlantasDensidad = (areaCultivo / (distanciaCalleMayor * distanciaPlantas));
    }

    void CountPlantsInCrop()
    {
        cantidadPlantas = areaCultivo * (5 / 1);
    }
}
