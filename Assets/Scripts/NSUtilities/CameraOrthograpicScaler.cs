﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSUtilities
{
    public class CameraOrthograpicScaler : MonoBehaviour
    {
        [SerializeField] private Camera refCamera;

        [SerializeField] private float maxAddOrthograpicSize = 1.9f;

        [SerializeField, Tooltip("Tamaño maximo de la pantalla")] private float maxWidthScreenSuported = 2700f;
        
        private float maxWidthScreenSuportedInternal;
        
        private void Awake()
        {
            maxWidthScreenSuportedInternal = maxWidthScreenSuported;
            maxWidthScreenSuported -= 1920;
            var tmpActualDifference = maxWidthScreenSuportedInternal - Screen.width;
            refCamera.orthographicSize = refCamera.orthographicSize + ((tmpActualDifference * maxAddOrthograpicSize) / maxWidthScreenSuported);
        }
    }
}