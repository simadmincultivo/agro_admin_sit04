﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


namespace NSUtilities
{
    public class UpdateLayoutChildrens : MonoBehaviour
    {
        private void OnEnable()
        {
            StartCoroutine(CouUpdate(GetComponent<RectTransform>()));
        }

        // Start is called before the first frame update
        private void UpdateLayout(RectTransform argTransformParent)
        {
            for (int i = 0; i < argTransformParent.childCount; i++)
            {
                argTransformParent.GetChild(i);
                UpdateLayout(argTransformParent.GetChild(i).GetComponent<RectTransform>());
                LayoutRebuilder.ForceRebuildLayoutImmediate(argTransformParent.GetComponent<RectTransform>());
            }
        }
        
        private IEnumerator CouUpdate(RectTransform argTransformParent)
        {
            yield return new WaitForSeconds(0.5f);
            UpdateLayout(argTransformParent);
        }
    }
}