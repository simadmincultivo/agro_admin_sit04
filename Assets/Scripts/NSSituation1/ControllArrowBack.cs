﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSActiveZone;
using UnityEngine.SceneManagement;
using NSInterfaz;
using NSBoxMessage;
using NSTraduccionIdiomas;

public class ControllArrowBack : AbstractObject2D
{
    private bool practicas = true;
    [Header("Objetos Practica 1")]
    [SerializeField] private GameObject assemblyComponent;
    [SerializeField] private GameObject backgroundGerminador;
    public bool selectedHangar;
    public bool selectedGerminadero;

    [Header("Objetos practica 2")]
    [SerializeField] private GameObject[] backgroundsP2;
    [SerializeField] private StrctureControllerP2 refStructureControllerP2;
    
    public bool selectedCellarP2;
    public bool selectedHangarP2;
    public bool selectedGreenHouseP2;
    public bool selectedGerminatorP2;
    [SerializeField] private GameObject[] botonesUnicos;

    [Header("Objetos practica 3")]
    [SerializeField] private GameObject[] BackgroundsP3;
    public bool selectedGreenHouseP3;
    public bool selectedCollectionCenter;
    public bool selectedSelectionAndPackaging;   

    public void ButtonBack()
    {
        if(practicas)
        {
            if(selectedHangar)
            {
                assemblyComponent.SetActive(false);
                UpdateActualStateAndActiveZones(1);
                selectedHangar = false;
            }
            else if(selectedGerminadero)
            {
                backgroundGerminador.SetActive(false);
                UpdateActualStateAndActiveZones(1);
                selectedGerminadero = false;
            }
            else if(selectedCellarP2)
            {
                backgroundsP2[0].SetActive(false);
                UpdateActualStateAndActiveZones(1);
                selectedCellarP2 = false;
                botonesUnicos[0].SetActive(false);
            }
            else if(selectedHangarP2)
            {
                backgroundsP2[1].SetActive(false);
                UpdateActualStateAndActiveZones(1);
                PanelIngreseVolumenAgua.Instance.ShowPanel(false);
                selectedHangarP2 = false;
                botonesUnicos[1].SetActive(false);
            }
            else if(selectedGreenHouseP2)
            {
                backgroundsP2[2].SetActive(false);
                selectedGreenHouseP2 = false;
                UpdateActualStateAndActiveZones(1);
                refStructureControllerP2.greenHouseInside = false;
            }
            else if(selectedGerminatorP2)
            {
                backgroundsP2[3].SetActive(false);
                selectedGerminatorP2 = false;
                PanelIngreseCantidadPlantulas.Instance.ShowPanel(false);
                UpdateActualStateAndActiveZones(1);
                refStructureControllerP2.germinatorInside = false;
            }
            else if(selectedGreenHouseP3)
            {
                BackgroundsP3[0].SetActive(false);
                selectedGreenHouseP3 = false;
                UpdateActualStateAndActiveZones(1);
            }
            else if(selectedCollectionCenter)
            {
                BackgroundsP3[1].SetActive(false);
                selectedCollectionCenter = false;
                PanelIngreseCantidadCanastillas.Instance.ShowPanel(false);
                UpdateActualStateAndActiveZones(1);
            }
            else if(selectedSelectionAndPackaging)
            {
                BackgroundsP3[2].SetActive(false);
                selectedSelectionAndPackaging = false;
                PanelIngreseCantidadBalanzas.Instance.ShowPanel(false);
                PanelIngreseCantidadCanastillas2.Instance.ShowPanel(false);
                ActivateActiveZone("ZoneBalanza", false);
                UpdateActualStateAndActiveZones(1);
            }
            else
            {
                //BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("mensajeSalirDeLaSituacion"), DiccionarioIdiomas.Instance.Traducir("TextCancelar"), DiccionarioIdiomas.Instance.Traducir("TextAceptar"), ButtonConfirmBack, null);
            }
        }
    }

    void ButtonConfirmBack()
    {
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        SceneManager.LoadScene("Menu", LoadSceneMode.Additive);
    }

    public void UpdateActualStateAndActiveZones(int state)
    {
        actualStageIndex = state;
        ActivateAllActiveZonesOfActualStage(true);
    }
    protected override void OnReleaseObjectOverActiveZone(string argNameActiveZone)
    {
    }

    protected override void OnReleaseObjectOverNothing()
    {
    }

    public override void OnTab()
    {
    }

    public override void OnTabOverActiveZone(string argNameActiveZone)
    {
    }

    public override void OnMoving()
    {
    }
}
