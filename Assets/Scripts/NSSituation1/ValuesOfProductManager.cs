﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValuesOfProductManager: MonoBehaviour
{
    [Header("Valores para los productos")]
    public int[] valuesStructureAndDoors;
    public int[] valuesCubierta;
    public int[] valuesVentilacion;
    public int[] valuesRefrigeracion;
    public int valueControlAmbiental;
    public int[] valuesAnalisisSuelo;
    public int[] valuesTanquesAgua;
    public int[] valuesFertirriego;
    public int priceJornal;
}
