﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSScriptableEvent;

public class activarBienvenida : MonoBehaviour
{
    public ScriptableEvent eventoCerrarVentana1;
   // public ScriptableEvent eventoCerrarVentana2;
    private void Start()
    {
        eventoCerrarVentana1.Subscribe(actiBienvenida);
       // eventoCerrarVentana2.Subscribe(actiBienvenida);
    }

    public void actiBienvenida()
    {
        GameObject.FindGameObjectWithTag("control").GetComponent<ControllInteractionsP4>().activarBienvenida();
    }
}
