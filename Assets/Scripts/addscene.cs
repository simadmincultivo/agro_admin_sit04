﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



    public class addscene : MonoBehaviour
    {
        [SerializeField]
        private GameObject menuINi;

        // Start is called before the first frame update
        void Start()
        {
            SceneManager.LoadScene("Practica4", LoadSceneMode.Additive);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void onMenuInicio()
        {
            menuINi.SetActive(true);
        }

        public void offMenuInicio()
        {
            menuINi.SetActive(false);
        }


    }
