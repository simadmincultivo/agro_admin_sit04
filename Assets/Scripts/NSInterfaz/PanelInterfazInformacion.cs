﻿using System.Collections;
using NSAvancedUI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using NSTraduccionIdiomas;

namespace NSInterfaz
{
    public class PanelInterfazInformacion : AbstractSingletonPanelUIAnimation<PanelInterfazInformacion>
    {
        #region  members

        [SerializeField] private TextMeshProUGUI textSituacion;
        [SerializeField] private RegisterOfDates myRegisterDatesP1;
        [SerializeField] private ValuesProductosManagerP2 myProductsManagerP2;
        [SerializeField] private ValuesOfProductsManagerP3 myProductsManagerP3;

        [SerializeField] private string practica1;
        [SerializeField] private string practica2;
        [SerializeField] private string practica3;
        [SerializeField] private string practica4;

        #endregion
        private void Start()
        {
            CallMethodSetValuesAleatorios();
        }

        public void CallMethodSetValuesAleatorios()
        {
            //StartCoroutine(SetValuesAleatorios());
            SetValuesAleatorios();
        }

        void SetValuesAleatorios()
        {
            //yield return new WaitForSeconds(0.1f);

            if(SceneManager.GetActiveScene().name.Equals(practica1))
            {
                var tmpTextRemplace = DiccionarioIdiomas.Instance.Traducir("TextoDescripcionBienvenidaP1").Replace("aaaaa", myRegisterDatesP1.valorParaRealizacion.ToString());
                textSituacion.text = tmpTextRemplace;
                LayoutRebuilder.ForceRebuildLayoutImmediate(textSituacion.transform.parent.GetComponent<RectTransform>());
            }
            else if(SceneManager.GetActiveScene().name.Equals(practica2))
            {
                var tmpTextRemplace = DiccionarioIdiomas.Instance.Traducir("TextoDescripcionBienvenidaP2");
                tmpTextRemplace = tmpTextRemplace.Replace("lll", myProductsManagerP2.largoCultivo.ToString());
                tmpTextRemplace = tmpTextRemplace.Replace("aaa", myProductsManagerP2.anchoCultivo.ToString());
                tmpTextRemplace = tmpTextRemplace.Replace("ccc", myProductsManagerP2.distanciasDeSurcoCalleMayorValue.ToString());
                tmpTextRemplace = tmpTextRemplace.Replace("ppp", myProductsManagerP2.distanciasDeSurcoPlantasValue.ToString());                
                textSituacion.text = tmpTextRemplace;
                LayoutRebuilder.ForceRebuildLayoutImmediate(textSituacion.transform.parent.GetComponent<RectTransform>());
            }
            else if(SceneManager.GetActiveScene().name.Equals(practica3))
            {
                var tmpTextRemplace = DiccionarioIdiomas.Instance.Traducir("TextoDescripcionBienvenidaP3");
                tmpTextRemplace = tmpTextRemplace.Replace("lll", myProductsManagerP3.largoCultivo.ToString());
                tmpTextRemplace = tmpTextRemplace.Replace("aaa", myProductsManagerP3.anchoCultivo.ToString());
                tmpTextRemplace = tmpTextRemplace.Replace("ccc", myProductsManagerP3.distanciaCalleMayor.ToString());
                tmpTextRemplace = tmpTextRemplace.Replace("ppp", myProductsManagerP3.distanciaEntrePlantas.ToString());
                tmpTextRemplace = tmpTextRemplace.Replace("csp", myProductsManagerP3.cantidadProduccidoPlanta.ToString("0.0"));
                tmpTextRemplace = tmpTextRemplace.Replace("ckc", myProductsManagerP3.kilogrmosPorCanastilla.ToString("0.0"));
                tmpTextRemplace = tmpTextRemplace.Replace("ptj", myProductsManagerP3.cantidadMalaCalidad.ToString());
                textSituacion.text = tmpTextRemplace;
                LayoutRebuilder.ForceRebuildLayoutImmediate(textSituacion.transform.parent.GetComponent<RectTransform>());
            }
        }
        /* 
        #region members

        [SerializeField] private ToggleGroup toggleGroup;

        [SerializeField] private Toggle toggleSituacion;
        
        [SerializeField] private Toggle toggleProcedimiento;
    
        [SerializeField] private Toggle toggleEcuaciones;
        #endregion

        private void Awake()
        {
            toggleGroup.RegisterToggle(toggleSituacion);
            toggleGroup.RegisterToggle(toggleProcedimiento);
            toggleGroup.RegisterToggle(toggleEcuaciones);
        }

        private void OnEnable()
        {
            toggleGroup.NotifyToggleOn(toggleSituacion);
        }

        #region public methods
        
        #endregion
        */
    }
}