﻿#pragma warning disable 0649
using NSAvancedUI;
using NSBoxMessage;
using NSSeguridad;
using NSTraduccionIdiomas;
using System;
using NSScriptableEvent;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NSInterfaz
{
    public class PanelInterfazLogin2Campos : AbstractSingletonPanelUIAnimation<PanelInterfazLogin2Campos>
    {
        #region members

        [Header("Datos login")]
        [Space(20)]
        [SerializeField] private TMP_InputField inputUsuario;

        [SerializeField] private TMP_InputField inputPassword;

        [SerializeField] private TextMeshProUGUI textUsuarioPlaceHolder;

        [SerializeField] private TextMeshProUGUI textPasswordPlaceHolder;

        [SerializeField] private TextMeshProUGUI textButtonIniciar;

        [SerializeField] private Button buttonIniciar;

        private string[] cmd;

        private string correoMono;

        private string nombreMono;

        private string instituMono;

        [SerializeField] private ScriptableEventInt seOnLogin;
        
        [SerializeField] private ScriptableEvent seOnInputsLogin2InputsCorrect;

        [SerializeField] private SOSecurityConfiguration refSoSecurityConfiguration;

        #endregion

        #region monoBehaviour

        // Use this for initialization
        private void Start()
        {
            textUsuarioPlaceHolder.text = DiccionarioIdiomas.Instance.Traducir("TextUsuario") + "...";
            textPasswordPlaceHolder.text = DiccionarioIdiomas.Instance.Traducir("TextContraseña") + "...";

            if (refSoSecurityConfiguration.isModeMonoUser)
            {
#if UNITY_STANDALONE
                // Obtenemos los argumentos enviados a la aplicación de escritorio.
                cmd = Environment.CommandLine.Split(',');

                try
                {
                    if (cmd.Length > 5)
                    {
                        nombreMono = cmd[3];
                        instituMono = cmd[4];
                        correoMono = cmd[5];
                    }
                }
                catch
                {
                    BoxMessageManager.Instance.CreateBoxMessageInfo("esto es lo que hay dentro de la cmd " + cmd, "ACEPTAR");
                }

#elif UNITY_ANDROID || UNITY_IPHONE
                try
                {
                    AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
                    AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

                    if (currentActivity != null)
                    {
                        AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");

                        if (intent != null)
                        {
                            nombreMono = safeCallStringMethod(intent, "getStringExtra", "nombre");
                            instituMono = safeCallStringMethod(intent, "getStringExtra", "institucion");
                            correoMono = safeCallStringMethod(intent, "getStringExtra", "correo");
                        }
                    }
				}
                catch (Exception e)
                {
					Debug.Log(e.ToString());
				}
#endif
                inputPassword.text = correoMono;
            }
        }

        private void OnEnable()
        {
            seOnLogin.Subscribe(mtdCallbackLogin);
        }
        
        private void OnDisable()
        {
            seOnLogin.Unsubscribe(mtdCallbackLogin);
        }

        // Update is called once per frame
        private void Update()
        {
#if UNITY_EDITOR || UNITY_STANDALONE||UNITY_ANDROID || UNITY_IPHONE
            if (refSoSecurityConfiguration.isModeMonoUser)
                inputUsuario.text = correoMono;
#endif
        }

        #endregion

        #region private methods

        /// <summary>
        /// metodo que se agrega al delegado de seguridad para saber la respuesta del la operacion loguin
        /// </summary>
        /// <param name="op"></param>
        private void mtdCallbackLogin(int op)
        {
            if (op == 1)
                ShowPanel(false);
            else
            {
                if (op == 0)
                {
                    textButtonIniciar.text = DiccionarioIdiomas.Instance.Traducir("TextIngresar");
                    buttonIniciar.enabled = true;
                }
            }
        }

        #endregion

        #region public methods

        public void LimpiarCampoUsuario()
        {
            textUsuarioPlaceHolder.text = "";
        }

        public void LimpiarCampoContrasenia()
        {
            textPasswordPlaceHolder.text = "";
        }

        /// <summary>
        /// metodo que verifica campos de loguin y pasa los datos a seguridad
        /// </summary>
        public void ButtonLogin()
        {
            if (inputUsuario.text != "" && inputPassword.text != "")
            {
                Debug.Log("entre a llamar aula loginRequest");
                seOnInputsLogin2InputsCorrect.ExecuteEvent();
                textButtonIniciar.text = DiccionarioIdiomas.Instance.Traducir("TextCargando");
                buttonIniciar.enabled = false;
            }
            else
            {
                BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeCamposNecesarios"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));
                //Inicio codigo Luis
                //PanelInterfazLogin2Campos.instance.ShowPanel();
                //Fin codigo Luis
            }
        }

        #endregion

        public static string safeCallStringMethod(AndroidJavaObject javaObject, string methodName, params object[] args)
        {
            if (args == null)
                args = new object[] {null};

            IntPtr methodID = AndroidJNIHelper.GetMethodID<string>(javaObject.GetRawClass(), methodName, args, false);
            jvalue[] jniArgs = AndroidJNIHelper.CreateJNIArgArray(args);

            try
            {
                IntPtr returnValue = AndroidJNI.CallObjectMethod(javaObject.GetRawObject(), methodID, jniArgs);

                if (IntPtr.Zero != returnValue)
                {
                    var val = AndroidJNI.GetStringUTFChars(returnValue);
                    AndroidJNI.DeleteLocalRef(returnValue);
                    return val;
                }
            }
            finally
            {
                AndroidJNIHelper.DeleteJNIArgArray(args, jniArgs);
            }

            return null;
        }
    }
}