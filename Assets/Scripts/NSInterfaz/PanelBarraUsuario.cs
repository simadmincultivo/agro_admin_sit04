﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NSCalificacionSituacion;
using NSScriptableEvent;
using NSSeguridad;

namespace NSInterfaz
{
    public class PanelBarraUsuario : MonoBehaviour
    {
        #region members

        [SerializeField] private Animator animatorBarraInformacionUsuario;
        
        [SerializeField] private SOSecurityData soSecurityData;

        [SerializeField] private TextMeshProUGUI textTiempoSesion;

        [SerializeField] private TextMeshProUGUI textCantidadIntentos;

        [SerializeField] private TextMeshProUGUI textNombreUsuario;

        [SerializeField] private ScriptableEventInt seAttemps;

        [SerializeField] private ScriptableEventString seTime;

        #endregion

        #region monoBehaviour

        void Awake()
        {
          //  ActualizarValorNombreUsuario(soSecurityData.aulaUsername);
        }

        private void OnEnable()
        {
            seAttemps.Subscribe(UpdateAttempts);
            seTime.Subscribe(UpdateTime);
        }

        private void OnDisable()
        {
            seAttemps.Unsubscribe(UpdateAttempts);
            seTime.Subscribe(UpdateTime);
        }

        #endregion

        #region public methods

        public void OnButtonDesplegarContraerMenu()
        {
            animatorBarraInformacionUsuario.SetBool("desplegar", !animatorBarraInformacionUsuario.GetBool("desplegar"));
        }
        
        public void ActualizarValorNombreUsuario(string argNombreUsuarioActual)
        {
            textNombreUsuario.text = argNombreUsuarioActual;
        }
        public void LoadAttempts(int newAttempts)
        {
            textCantidadIntentos.text = newAttempts.ToString();
        }
        public int GetAttempts()
        {
            return  Int32.Parse(textCantidadIntentos.text);
        }
        #endregion

        #region private methods

        private void UpdateTime(string argNewTime)
        {
            textTiempoSesion.text = argNewTime;
            ActualizarValorNombreUsuario(soSecurityData.aulaUsername);
        }

        private void UpdateAttempts(int argAttempts)
        {
            textCantidadIntentos.text = argAttempts.ToString();
        }
        #endregion
    }
}