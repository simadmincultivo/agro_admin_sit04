using System;
using UnityEngine;

namespace NSInterfaz
{
    public class PanelInterfazVentanaObscura : MonoBehaviour
    {
        #region members

        public Action DelegateClosePanel = () => {};

        private bool interactuable = true;

        #endregion
        
        #region accesors

        public bool _interactuable
        {
            get { return interactuable; }
            set { interactuable = value; }
        }
        #endregion

        #region public methods
        
        public void OnClick()
        {
            if (interactuable)
                DelegateClosePanel();            
        }
        #endregion
    }
}